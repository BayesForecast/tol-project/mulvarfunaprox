/* -*- mode: C++ -*- */
//////////////////////////////////////////////////////////////////////////////
// FILE   : DesignC2.tol
// PURPOSE: Defines Class @DesignC2
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.DesignC2.Monomial = 
"";
Class @DesignC2.Monomial : @DesignC2
//////////////////////////////////////////////////////////////////////////////
{
//////////////////////////////////////////////////////////////////////////////
//Configuration parameters
//////////////////////////////////////////////////////////////////////////////
  Real max.deg = 6;
  Real use.compact.base = False;
//////////////////////////////////////////////////////////////////////////////
//Auxiliar members
//////////////////////////////////////////////////////////////////////////////
  Real _.K0 = ?;

  //////////////////////////////////////////////////////////////////////////////
    Real build.base(Real void)
  //////////////////////////////////////////////////////////////////////////////
  {
    Text _MID = "[@DesignC2.Monomial::build.base] ";
    Real max.K = Max(1,Min(_.K0, Floor(_.M/(_.H*min.contrast.surface))));
    FunRn2R::@BaseC2 base.feasible = If(use.compact.base,
    {
      FunRn2R::@BaseC2.LinComb::Orthogonal.Rand(_.base[1],_.K0)
    },
    {
      WriteLn("TRACE "+_MID+"1.1 K0="<<_.K0);
      WriteLn("TRACE "+_MID+"1.2 M="<<_.M);
      WriteLn("TRACE "+_MID+"1.3 H="<<_.H);
      WriteLn("TRACE "+_MID+"1.4 SC="<<min.contrast.surface);
      WriteLn("TRACE "+_MID+"1.5 max.K="<<max.K);
      Real deg = 
      {
        Real deg = 0;
        Real K = 1;
        Set pair.deg.K = [[ [[Copy(deg),Copy(K),1]] ]];
        While(And(deg<=max.deg, K<=max.K),
        {
          Real deg := deg+1;
          Real k = Comb(_.n+deg-1,deg);
          Real K := K+k;
          WriteLn("TRACE "+_MID+"2."<<deg+" K="<<K);
          Set Append(pair.deg.K, [[ [[Copy(deg),Copy(K),Copy(k)]] ]])
        });
        If(pair.deg.K[deg+1][2]<=max.K,deg,deg-1)
      };
      FunRn2R::@BaseC2.Monomial::New(_.n,deg)
    });
    Real _.K := base.feasible::_.K;
    VMatrix _.Yn := If(use.compact.base,
    {
      Floor(Min(Rand(_.H,1,_.K,_.K),Max(_.Ym/min.contrast.surface,_.Ym*0+1)))
    },
    {
      Rand(_.H,1,_.K,_.K)
    }); 
    Set Append(_.base, [[base.feasible]]);
    Real _.base.sel := Card(_.base);
    _.K
  };

  //////////////////////////////////////////////////////////////////////////////
    Real build.model.design(Real void)
  //////////////////////////////////////////////////////////////////////////////
  {
    Text _MID = "[@DesignC2.Monomial::build.model.design] ";
    FunRn2R::@BaseC2 base.max = 
      FunRn2R::@BaseC2.Monomial::New(_.n,max.deg);
    Set Append(_.base, [[base.max]]);
    Real _.base.sel := Card(_.base);
    Real _.K0 := base.max::_.K;
    _build.model.design(void)
  }

};
